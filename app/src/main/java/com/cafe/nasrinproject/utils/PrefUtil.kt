package com.cafe.nasrinproject.utils

import android.content.Context
import android.content.SharedPreferences

class PrefUtil {
    companion object {

        private const val PREFS_FILENAME = "com.cafe.nasrinproject"
        private lateinit var prefUtils: SharedPreferences

        private fun startPref(context: Context) {
            prefUtils = context.getSharedPreferences(PREFS_FILENAME, 0)
        }

        fun setString(name: String, value: String?, context: Context) {
            startPref(context)
            prefUtils.edit().putString(name, value).apply()
        }

        fun getString(name: String, context: Context): String {
            startPref(context)
            return prefUtils.getString(name, "")!!
        } fun getString(name: String, context: Context, default: String): String {
            startPref(context)
            return prefUtils.getString(name, default)!!
        }

        fun getBoolean(name: String, context: Context, default: Boolean): Boolean {
            startPref(context)
            return prefUtils.getBoolean(name, default)
        }

        fun setBoolean(name: String, context: Context, default: Boolean) {
            startPref(context)
            prefUtils.edit().putBoolean(name, default).apply()
        }

        fun clear(context: Context) {
            startPref(context)
            prefUtils.edit().clear().apply()
        }
    }
}