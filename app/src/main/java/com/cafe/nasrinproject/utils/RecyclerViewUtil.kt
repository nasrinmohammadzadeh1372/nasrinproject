package com.cafe.nasrinproject.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

object RecyclerViewUtil {

  @JvmStatic
  fun scrollDetected(layoutManager: RecyclerView.LayoutManager, dy: Int): Boolean {
    if (dy > 0) {
      val visibleItemCount = layoutManager.childCount
      val totalItemCount = layoutManager.itemCount
      val pastVisibleItems =
        (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
      if (visibleItemCount + pastVisibleItems >= totalItemCount) {
        return true
      }
    }
    return false
  }
}
