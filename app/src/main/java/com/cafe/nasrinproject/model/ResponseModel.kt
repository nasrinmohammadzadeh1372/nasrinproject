package com.cafe.nasrinproject.model

import java.util.ArrayList

data class ResponseModel(
    val meta: Meta,
    val response: Response
)

data class Meta (
    val code: Long,
    val errorType: String,
    val errorDetail: String,
)

data class Response (
    val totalResults: Long,
    val groups: List<Group>
)

data class Group (
    val type: String,
    val name: String,
    val items: ArrayList<GroupItem>
)

data class GroupItem (
    val reasons: Reasons,
    val venue: Venue
)

data class Reasons (
    val count: Long,
    val items: ArrayList<ReasonsItem>
)

data class ReasonsItem (
    val summary: String,
    val type: String,
    val reasonName: String
)

data class Venue (
    val id: String,
    val name: String,
    val location: Location,
    val categories: List<Category>?,
    val popularityByGeo: Double,
    val venuePage: VenuePage
)

data class Category (
    val id: String,
    val name: String,
    val pluralName: String,
    val shortName: String,
    val icon: Icon,
    val primary: Boolean
)

data class Icon (
    val prefix: String,
    val suffix: String
)

data class Location (
    val address: String,
    val crossStreet: String,
    val lat: Double,
    val lng: Double,
    val labeledLatLngs: List<LabeledLatLng>,
    val distance: Long,
    val postalCode: String,
    val cc: String,
    val city: String,
    val state: String,
    val country: String,
    val formattedAddress: List<String>
)

data class LabeledLatLng (
    val label: String,
    val lat: Double,
    val lng: Double
)

data class VenuePage (
    val id: String
)
