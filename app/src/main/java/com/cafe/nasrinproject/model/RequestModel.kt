package com.cafe.nasrinproject.model

data class RequestModel(
    var lat: String? = null,
    var lng: String? = null
)
