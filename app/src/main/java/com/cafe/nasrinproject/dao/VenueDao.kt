package com.cafe.nasrinproject.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface VenueDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(venues: VenueEntity): Long

    @Query("SELECT * FROM venue LIMIT :pageNumber,:size")
    fun selectAll(pageNumber: Int,size: Int): List<VenueEntity>
}