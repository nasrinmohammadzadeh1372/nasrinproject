package com.cafe.nasrinproject.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "venue")
data class VenueEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "address")
    var address: String?,

    @ColumnInfo(name = "distance")
    var distance: Long?,

    @ColumnInfo(name = "lat")
    var lat: String?,

    @ColumnInfo(name = "lng")
    var lng: String?,

    @ColumnInfo(name = "categoryName")
    var categoryName: String?
):Serializable