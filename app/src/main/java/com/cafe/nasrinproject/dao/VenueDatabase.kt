package com.cafe.nasrinproject.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [VenueEntity::class], version = 1)
abstract class VenueDatabase : RoomDatabase() {

    abstract fun venueDao(): VenueDao

    companion object {
        private var instance: VenueDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): VenueDatabase {
            if(instance == null)
                instance = Room.databaseBuilder(ctx.applicationContext, VenueDatabase::class.java,
                    "venue_db")
                    .fallbackToDestructiveMigration()
                    .build()

            return instance!!

        }

    }

}
