package com.cafe.nasrinproject.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cafe.nasrinproject.R
import com.cafe.nasrinproject.dao.VenueEntity
import com.cafe.nasrinproject.databinding.LocationListItemBinding

class LocationListAdapter : ListAdapter<VenueEntity, LocationListAdapter.ItemHolder>(DIFF_CALLBACK) {
  private var listener: OnItemClickListener? = null

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
    val binding: LocationListItemBinding = DataBindingUtil.inflate(
      LayoutInflater.from(parent.context),
      R.layout.location_list_item,
      parent,
      false)
    return ItemHolder(binding)
  }

  override fun submitList(list: List<VenueEntity>?) {
    super.submitList(if (list != null) ArrayList(list) else null)
  }

  override fun onBindViewHolder(holder: ItemHolder, position: Int) {
    holder.binding.item = getItem(position)
  }

  fun setOnItemClickListener(listener: OnItemClickListener) {
    this.listener = listener
  }

  interface OnItemClickListener {
    fun onItemClick(item: VenueEntity, position: Int)
  }

  inner class ItemHolder(val binding: LocationListItemBinding) : RecyclerView.ViewHolder(binding.root) {
    init {
      itemView.setOnClickListener {
        val position = absoluteAdapterPosition
        if (listener != null && position != RecyclerView.NO_POSITION) {
          val item = getItem(position)
          listener!!.onItemClick(item, position)
        }
      }
    }
  }

  companion object {
    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<VenueEntity>() {
      override fun areItemsTheSame(oldItem: VenueEntity, newItem: VenueEntity): Boolean {
        return oldItem.id == newItem.id
      }

      override fun areContentsTheSame(oldItem: VenueEntity, newItem: VenueEntity): Boolean {
        return oldItem == newItem
      }
    }
  }
}
