package com.cafe.nasrinproject.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.cafe.nasrinproject.R
import com.cafe.nasrinproject.network.NetworkErrorEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainActivity : AppCompatActivity() {
    private var eventBusRegistered = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: NetworkErrorEvent) {
        Toast.makeText(applicationContext,event.error,Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()
        if (!eventBusRegistered) {
            EventBus.getDefault().register(this)
            eventBusRegistered = true
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        eventBusRegistered = false
        super.onDestroy()
    }
}