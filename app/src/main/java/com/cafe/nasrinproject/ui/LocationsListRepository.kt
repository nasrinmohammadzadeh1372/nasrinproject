package com.cafe.nasrinproject.ui

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.cafe.nasrinproject.dao.VenueDao
import com.cafe.nasrinproject.dao.VenueDatabase
import com.cafe.nasrinproject.dao.VenueEntity
import com.cafe.nasrinproject.model.GroupItem
import com.cafe.nasrinproject.model.RequestModel
import com.cafe.nasrinproject.network.NetworkUtil
import com.cafe.nasrinproject.network.RetrofitBuilder
import com.cafe.nasrinproject.utils.Constants.client_id
import com.cafe.nasrinproject.utils.Constants.client_secret
import com.cafe.nasrinproject.utils.Constants.version
import com.cafe.nasrinproject.utils.RequestStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class LocationsListRepository(application: Application) {

    var result: MutableLiveData<ArrayList<VenueEntity>> = MutableLiveData()
    var status: MutableLiveData<Int> = MutableLiveData()
    private var loading = false
    private var endOfList = false
    private var size = 0
    private var offset = 0
    private lateinit var request: RequestModel
    private var venueDao: VenueDao = VenueDatabase.getInstance(application).venueDao()
    private var isNetworkRequest = true


    fun list(offset: Int, request: RequestModel, isNetworkRequest: Boolean) {
        this.offset = offset
        this.size = 20
        this.request = request
        loading = true
        endOfList = false
        this.isNetworkRequest = isNetworkRequest


        CoroutineScope(NetworkUtil.getCoroutineContext(status = status)).launch {
            status.postValue(RequestStatus.LOADING.get())
            if (isNetworkRequest) {
                val response = RetrofitBuilder.locationListApi.getLocations(
                    client_id = client_id,
                    client_secret = client_secret,
                    ll = "${request.lat},${request.lng}",
                    v = version,
                    limit = size,
                    offset = offset
                )
                if (response != null) {
                    status.postValue(RequestStatus.CALL_SUCCESS.get())
                    val convertedList = ArrayList<VenueEntity>()
                    for (item in response.response.groups[0].items) {
                        convertedList.add(convertEntity(item))
                    }
                    handleListResult(convertedList)
                } else {
                    status.postValue(RequestStatus.CALL_FAILURE.get())
                }

            } else {
                status.postValue(RequestStatus.CALL_SUCCESS.get())
                handleListResult(venueDao.selectAll(offset,size) as ArrayList<VenueEntity>)
            }
        }
    }

    fun getNextPage() {
        if (!endOfList) {
            if (!loading) {
                list(
                    offset = offset + size,
                    request = request,
                    isNetworkRequest
                )
            }
        }
    }

    private fun handleListResult(data: ArrayList<VenueEntity>) {
        loading = false

        if (isFirstPage(offset)) {
            result.postValue(data)
        } else {
            result.value?.addAll(data)
            result.postValue(result.value)
        }
        endOfList = isEndOfList(data)
        insertInToDb(data)
    }

    private fun isEndOfList(list: List<*>?): Boolean {
        if (list == null || list.isEmpty() || list.size < size) {
            return true
        }
        return false
    }

    private fun isFirstPage(index: Int): Boolean = index == 0

    private fun insertInToDb(data: ArrayList<VenueEntity>) {
        for (item in data) {
            venueDao.insert(item)
        }
    }

    private fun convertEntity(data: GroupItem): VenueEntity {
        val venue = data.venue
        return VenueEntity(
            id = venue.id,
            name = venue.name,
            address = venue.location.address,
            distance = venue.location.distance,
            lat = venue.location.lat.toString(),
            lng = venue.location.lng.toString(),
            categoryName = venue.categories?.let { it[0].shortName }
        )
    }
}
