package com.cafe.nasrinproject.ui

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.cafe.nasrinproject.R
import com.cafe.nasrinproject.dao.VenueEntity
import com.cafe.nasrinproject.databinding.FragmentLocationListBinding
import com.cafe.nasrinproject.model.RequestModel
import com.cafe.nasrinproject.utils.PageableListStatus
import com.cafe.nasrinproject.utils.RecyclerViewUtil
import com.cafe.nasrinproject.utils.RequestStatus

class ListFragment : Fragment(), LocationListener {

    lateinit var viewModel: LocationListViewModel
    lateinit var binding: FragmentLocationListBinding
    private var isFirstLoading = false
    private var isRefresh = false
    private val requestModel = RequestModel()
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_location_list, container, false)
        viewModel = ViewModelProvider(this).get(LocationListViewModel::class.java)
        setViewStatus(status = PageableListStatus.LOADING.get())

        getLocation()
        binding()
        configNetworkStatus()
        return binding.root
    }


    private fun binding() {
        val adapter = LocationListAdapter()
        binding.recycler.adapter = adapter
        binding.recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (RecyclerViewUtil.scrollDetected(recyclerView.layoutManager!!, dy)) {
                    viewModel.getNextPage()
                }
            }
        })
        adapter.setOnItemClickListener(object : LocationListAdapter.OnItemClickListener {
            override fun onItemClick(item: VenueEntity, position: Int) {
                Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(
                    ListFragmentDirections.actionListFragmentToDetailFragment(item)
                )
            }
        })

        binding.ivRetry.setOnClickListener {
            callApi(true)
            isRefresh = true
        }

        binding.swipeRefresh.setOnRefreshListener {
            callApi(true)
            isRefresh = true
        }

        viewModel.result().observe(viewLifecycleOwner, {
            if (it != null) {
                binding.swipeRefresh.isRefreshing = false
                isFirstLoading = false

                if (isRefresh) {
                    isRefresh = false
                    val handler = Handler(Looper.getMainLooper())
                    handler.postDelayed({
                        binding.recycler.smoothScrollToPosition(0)
                    }, 100)
                }
                if (it.isEmpty()) {
                    setViewStatus(status = PageableListStatus.NO_RESULT.get())
                } else {
                    setViewStatus(status = PageableListStatus.LIST.get())
                }
                adapter.submitList(it)
            }
        })
    }

    private fun callApi(isForceRefresh: Boolean) {
        isFirstLoading = true
        viewModel.getFirstPage(requestModel, isForceRefresh)
    }

    private fun configNetworkStatus() {
        viewModel.status().observe(viewLifecycleOwner, {
            if (it != null) {
                when (it) {
                    RequestStatus.LOADING.get() -> {
                        if (isFirstLoading)
                            setViewStatus(status = PageableListStatus.LOADING.get())
                    }
                    RequestStatus.CALL_SUCCESS.get() -> {
                        setViewStatus(status = PageableListStatus.LIST.get())
                    }
                    RequestStatus.CALL_FAILURE.get() -> {
                        setViewStatus(status = PageableListStatus.RETRY.get())
                    }
                }
            }
        })
    }

    private fun setViewStatus(status: Int) {
        binding.progress.visibility = View.GONE
        binding.ivRetry.visibility = View.GONE
        binding.textEmpty.visibility = View.GONE
        when (status) {
            PageableListStatus.NOTHING.get() -> {
            }
            PageableListStatus.LOADING.get() -> {
                binding.swipeRefresh.visibility = View.GONE
                binding.progress.visibility = View.VISIBLE
            }
            PageableListStatus.LIST.get() -> {
                binding.swipeRefresh.visibility = View.VISIBLE
            }
            PageableListStatus.RETRY.get() -> {
                binding.swipeRefresh.visibility = View.GONE
                binding.ivRetry.visibility = View.VISIBLE
            }
            PageableListStatus.NO_RESULT.get() -> {
                binding.swipeRefresh.visibility = View.GONE
                binding.textEmpty.visibility = View.VISIBLE
            }
        }
    }

    private fun getLocation() {
        locationManager =
            (requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager)
        if ((ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationPermissionCode
            )
        }

        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            2000,
            1f,
            this
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == locationPermissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Permission Granted", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        requestModel.lat = location.latitude.toString()
        requestModel.lng = location.longitude.toString()
        callApi(false)

    }
}
