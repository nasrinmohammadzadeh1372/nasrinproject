package com.cafe.nasrinproject.ui

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.cafe.nasrinproject.dao.VenueEntity
import com.cafe.nasrinproject.model.RequestModel
import com.cafe.nasrinproject.utils.Constants
import com.cafe.nasrinproject.utils.NetworkManager
import com.cafe.nasrinproject.utils.PrefUtil

class LocationListViewModel(application: Application) : AndroidViewModel(application) {
  private var repository = LocationsListRepository(application)

  fun status(): MutableLiveData<Int> {
    return repository.status
  }

  fun result(): MutableLiveData<ArrayList<VenueEntity>> {
    return repository.result
  }

  fun getFirstPage(request: RequestModel,forceRefresh: Boolean) {
    val savedLocation = getLastSavedLocation()
    val currentLocation = Location("")
    currentLocation.latitude = request.lat!!.toDouble()
    currentLocation.longitude = request.lng!!.toDouble()

    if (((savedLocation.latitude == 0.0 && savedLocation.longitude == 0.0) || savedLocation.distanceTo(currentLocation) > 100 || result().value == null || forceRefresh) && NetworkManager.isNetworkAvailable(getApplication())){
      saveLastLocation(currentLocation)
      repository.list(
        offset = 0,
        request = request,
        isNetworkRequest = NetworkManager.isNetworkAvailable(getApplication())
      )
    }else{
      if (result().value == null) {
        repository.list(
          offset = 0,
          request = request,
          isNetworkRequest = false
        )
      }
    }

  }

  fun getNextPage() {
    repository.getNextPage()
  }

  private fun getLastSavedLocation():Location{
    val lat = PrefUtil.getString(Constants.latitude, getApplication(), "0")
    val lng = PrefUtil.getString(Constants.longitude, getApplication(), "0")

    val lastLoc = Location("")
    lastLoc.latitude = lat.toDouble()
    lastLoc.longitude = lng.toDouble()
    return lastLoc
  }

  private fun saveLastLocation(location: Location){
    PrefUtil.setString(Constants.latitude,location.latitude.toString(), getApplication())
    PrefUtil.setString(Constants.longitude,location.longitude.toString(), getApplication())
  }
}
