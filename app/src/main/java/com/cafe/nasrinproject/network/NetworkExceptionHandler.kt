package com.cafe.nasrinproject.network

import androidx.lifecycle.MutableLiveData
import com.cafe.nasrinproject.utils.RequestStatus
import kotlinx.coroutines.CoroutineExceptionHandler

class NetworkExceptionHandler {
  fun getInstance(status: MutableLiveData<Int>? = null) = CoroutineExceptionHandler { _, _ ->
    status?.postValue(RequestStatus.CALL_FAILURE.get())
  }
}