package com.cafe.nasrinproject.network

import com.cafe.nasrinproject.model.ResponseModel
import retrofit2.http.GET
import retrofit2.http.Query

interface LocationListApi {
    @GET("v2/venues/explore")
    suspend fun getLocations(
        @Query("client_id") client_id: String?,
        @Query("client_secret") client_secret: String?,
        @Query("ll") ll: String?,
        @Query("v") v: String?,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): ResponseModel?
}