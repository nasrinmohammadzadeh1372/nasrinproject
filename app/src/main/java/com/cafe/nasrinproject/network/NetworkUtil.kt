package com.cafe.nasrinproject.network

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import kotlin.coroutines.CoroutineContext

object NetworkUtil {
  fun getRequestBody(body: JSONObject): RequestBody{
    return RequestBody.create(MediaType.parse("application/json"), body.toString())
  }

  fun getCoroutineContext(status: MutableLiveData<Int>? = null): CoroutineContext{
    return Dispatchers.IO + NetworkExceptionHandler().getInstance(status = status)
  }
}
