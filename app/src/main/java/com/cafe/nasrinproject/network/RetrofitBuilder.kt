package com.cafe.nasrinproject.network

import com.cafe.nasrinproject.utils.Constants.BASE_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(build())
            .build()
    }

    fun build(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(headerInterceptor())
        .addInterceptor(logInterceptor())
        .addInterceptor(responseInterceptor)
        .build()

    private fun logInterceptor(): Interceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    private fun headerInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method(), original.body())
                .addHeader("Accept-Encoding", "identity")
                .build()
            chain.proceed(request)
        }
    }

    private var responseInterceptor = Interceptor { chain ->
        val request = chain.request()
        val response = chain.proceed(request)
        when (response.code()) {
            200 -> return@Interceptor response
            else -> {
                val meta = JSONObject(DataConverter().getStringFromInputStream(response.body()?.byteStream()))
                val detail = JSONObject(meta.get("meta").toString())
                var error = "Network Error"
                if (!detail.isNull("errorType")){
                    error = detail.get("errorType").toString()
                }
                EventBus.getDefault().post(NetworkErrorEvent(error))
                return@Interceptor response
            }
        }
    }

    val locationListApi: LocationListApi = getRetrofit().create(LocationListApi::class.java)
}